запуск тестов:

IDE
* Собрать проект через Gradle
* В настройках конфигурации указать переменную окружение profile=test.gradle

Docker
* docker build .
* docker run -e profile=test.gradle <container id>