package dtoModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class PostListDto {
    List<PostDto> items;
    Integer page;
    Integer pages;
    @JsonProperty("per_page")
    Integer perPage;
    Integer total;
}
