package dtoModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import helpers.HelperDataGenerator;
import lombok.*;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class PostDto {
    private String id = "1";
    private String title;
    private String body;

    @JsonProperty("pub_date")
    private String pubDate;

    @JsonProperty("category_id")
    private Integer categoryId;

    private String category;

    public void setCustomPubDate(OffsetDateTime date) {
        this.pubDate = date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public PostDto(PostDto postDto) {
        this.id = postDto.getId();
        this.pubDate = postDto.getPubDate();
        this.body = postDto.getBody();
        this.category = postDto.getCategory();
        this.categoryId = postDto.getCategoryId();
        this.title = postDto.getTitle();
    }

    public PostDto(String id) {
        this.pubDate = OffsetDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        this.body = HelperDataGenerator.generateRandomString(200);
        this.category = HelperDataGenerator.generateRandomString();
        this.categoryId = 1;
        this.title = HelperDataGenerator.generateRandomString();
    }
}
