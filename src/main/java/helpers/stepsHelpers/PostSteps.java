package helpers.stepsHelpers;

import lombok.Getter;
import stepsApi.blog.PostGeneralSteps;
import stepsApi.blog.PostUpdateNegativeSteps;
import stepsApi.blog.PostUpdatePositiveSteps;

@Getter
public class PostSteps {
    PostGeneralSteps postGeneralSteps = new PostGeneralSteps();
    PostUpdatePositiveSteps postUpdatePositiveSteps = new PostUpdatePositiveSteps();
    PostUpdateNegativeSteps postUpdateNegativeSteps = new PostUpdateNegativeSteps();
}
