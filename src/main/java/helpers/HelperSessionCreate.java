package helpers;


import Utils.UtilProperty;
import enums.Specs;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class HelperSessionCreate {

    public static RequestSpecification createSession() {
        RestAssured.baseURI = UtilProperty.getProperty("BASE_URL");

        return RestAssured
                .given().spec(Specs.DEBUG.getSpecs())
                ;
    }
}
