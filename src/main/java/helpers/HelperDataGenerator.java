package helpers;

import java.util.Random;
public class HelperDataGenerator {
    public static String generateRandomString() {
        return generateRandomString(20);
    }

    public static String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < length; i++) {
            sb.append(
                    getEnglishSymbols()
                            .toCharArray()[
                                    random.nextInt(getEnglishSymbols()
                                            .length()
                                    )
                            ]
            );
        }

        return sb.toString();
    }

    public static String getEnglishSymbols() {
        return "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    }
}
