package stepsApi.blog;

import dtoModels.PostDto;
import helpers.stepsHelpers.HelperStepsConstructor;
import org.apache.http.HttpStatus;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PostUpdatePositiveSteps {

    public void stepUpdateAndCheckResult(PostDto postDto, HelperStepsConstructor steps) {
        steps.getPostSteps().getPostGeneralSteps().updateBlogRequest(postDto)
                .then().statusCode(HttpStatus.SC_NO_CONTENT);

        assertTrue(
        steps
                .getPostSteps().getPostGeneralSteps().getAllPostsRequest().stream()
                .anyMatch(postDto::equals),
                "Измененный пост не найден в общем списке"
        );
    }
}
