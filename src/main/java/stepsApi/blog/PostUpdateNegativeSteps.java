package stepsApi.blog;

import dtoModels.PostDto;
import helpers.stepsHelpers.HelperStepsConstructor;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PostUpdateNegativeSteps {
    public void checkPostHasNotBeenUpdate(PostDto postDto, HelperStepsConstructor steps) {
        assertTrue(
                steps
                        .getPostSteps().getPostGeneralSteps().getAllPostsRequest().stream()
                        .anyMatch(postDto::equals),
                "Выполнено недопустимое изменение в сущности post"
        );
    }
}
