package stepsApi.blog;

import com.sun.istack.NotNull;
import dtoModels.PostDto;
import dtoModels.PostListDto;
import enums.endpoints.posts.EndPointsPosts;
import helpers.HelperSessionCreate;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.List;

public class PostGeneralSteps {

    public List<PostDto> getAllPostsRequest() {
        return HelperSessionCreate
                .createSession()
                .get(EndPointsPosts.GET_POSTS.getEndpoint())
                .as(PostListDto.class)
                .getItems();
    }

    public Response updateBlogRequest(@NotNull PostDto postDto, String id) {
        return HelperSessionCreate
                .createSession().pathParam("id", id==null ? postDto.getId(): id)
                .contentType(ContentType.JSON)
                .body(postDto)
                .put(EndPointsPosts.PUT_POST.getEndpoint());
    }
    public Response updateBlogRequest(@NotNull PostDto postDto) {
        return updateBlogRequest(postDto, null);
    }
}
