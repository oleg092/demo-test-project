package Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;

public class UtilProperty {
    private static LinkedList<Properties> propertiesList = null;

    private static LinkedList<Properties> createProperties() throws IOException {
        propertiesList = new LinkedList<>();
        propertiesList.add(System.getProperties());

        String[] configFileNames = System.getenv("profile").split(";");
        for(String fileName: configFileNames) {
            Properties configuration = new Properties();
            configuration.load(new FileInputStream(fileName));
            propertiesList.addFirst(configuration);
        }

        return propertiesList;
    }

    public static String getProperty(String property) {
        if (propertiesList == null) {
            try {
                propertiesList = createProperties();
            }
            catch (Exception e) {
                new Throwable("Config file not found /" + e).printStackTrace();
            }
        }

        for(Properties properties: propertiesList) {
            String result = properties.getProperty(property, null);
            if(result != null) {
                return result;
            }
        }
        throw new NullPointerException(String.format("Конфиг с ключем %s не найден", property));
    }
}
