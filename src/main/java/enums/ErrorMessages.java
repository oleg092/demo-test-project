package enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorMessages {
    NOT_FOUND_MESSAGE("A database result was required but none was found.");

    private final String errorMessage;
}
