package enums.endpoints.posts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EndPointsPosts {
    GET_POSTS("blog/posts/"),
    PUT_POST("blog/posts/{id}");

    private final String endpoint;
}
