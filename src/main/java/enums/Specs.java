package enums;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Specs {
    DEBUG(new RequestSpecBuilder().log(LogDetail.ALL).build());

    private final RequestSpecification specs;
}
