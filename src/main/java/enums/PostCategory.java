package enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PostCategory {
    SCI_FI(1, "Sci-Fi"),
    POLITICS(2, "Politics"),
    TECH(3, "Tech");

    private final Integer id;
    private final String name;
}
