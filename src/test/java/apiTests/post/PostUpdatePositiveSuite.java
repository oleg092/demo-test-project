package apiTests.post;

import dtoModels.PostDto;
import enums.PostCategory;
import helpers.stepsHelpers.HelperStepsConstructor;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

@Tag("PostUpdatePositive")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PostUpdatePositiveSuite {
    HelperStepsConstructor steps = new HelperStepsConstructor();
    PostDto testBlog;

    @BeforeAll
    void preparing() {
        testBlog = steps
                .getPostSteps().getPostGeneralSteps().getAllPostsRequest()
                .stream().findFirst().get();
    }

    @AfterAll
    void endTests() {
        steps
                .getPostSteps().getPostGeneralSteps()
                .updateBlogRequest(testBlog).then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    @Tag("t1")
    public void bodyUpdateTest() {
        PostDto newDto = new PostDto(testBlog);
        newDto.setBody("new body");
        steps
                .getPostSteps().getPostUpdatePositiveSteps()
                .stepUpdateAndCheckResult(newDto, steps);
    }

    @Test
    @Tag("t2")
    public void titleUpdateTest() {
        PostDto newDto = new PostDto(testBlog);
        newDto.setTitle("new title");
        steps
                .getPostSteps().getPostUpdatePositiveSteps()
                .stepUpdateAndCheckResult(newDto, steps);
    }

    @Test
    @Tag("t3")
    public void categoryUpdateTest() {
        PostDto newDto = new PostDto(testBlog);
        newDto.setCategoryId(PostCategory.POLITICS.getId());
        newDto.setCategory(PostCategory.POLITICS.getName());
        steps
                .getPostSteps().getPostUpdatePositiveSteps()
                .stepUpdateAndCheckResult(newDto, steps);
    }
}
