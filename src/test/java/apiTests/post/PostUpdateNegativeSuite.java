package apiTests.post;

import dtoModels.PostDto;
import enums.ErrorMessages;
import helpers.stepsHelpers.HelperStepsConstructor;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import static org.hamcrest.Matchers.equalTo;

@Tag("PostUpdateNegative")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PostUpdateNegativeSuite {
    HelperStepsConstructor steps = new HelperStepsConstructor();
    PostDto testBlog;

    @BeforeAll
    void preparing() {
        testBlog = steps
                .getPostSteps().getPostGeneralSteps().getAllPostsRequest()
                .stream().findFirst().get();
    }

    @AfterAll
    void endTests() {
        steps
                .getPostSteps().getPostGeneralSteps()
                .updateBlogRequest(testBlog).then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    @Tag("t4")
    void notFoundPostTest() {
        steps
                .getPostSteps().getPostGeneralSteps()
                .updateBlogRequest(testBlog, "122121212").then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .and().body("message", equalTo(ErrorMessages.NOT_FOUND_MESSAGE.getErrorMessage()));
    }

    @Test
    @Tag("t5")
    void notFoundCategoryTest() {
        PostDto newDto = new PostDto(testBlog);
        newDto.setCategoryId(100500);

        steps
                .getPostSteps().getPostGeneralSteps()
                .updateBlogRequest(newDto).then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .and().body("message", equalTo(ErrorMessages.NOT_FOUND_MESSAGE.getErrorMessage()));

        steps.getPostSteps().getPostUpdateNegativeSteps().checkPostHasNotBeenUpdate(testBlog, steps);
    }

    @Test
    @Tag("t6")
    void incorrectDateFormatTest() {
        PostDto newDto = new PostDto(testBlog);
        newDto.setPubDate("2020-10-10");

        steps
                .getPostSteps().getPostGeneralSteps()
                .updateBlogRequest(newDto).then()
                .statusCode(HttpStatus.SC_NO_CONTENT); //ай яй яй =)))

        steps.getPostSteps().getPostUpdateNegativeSteps().checkPostHasNotBeenUpdate(testBlog, steps);
    }
}
